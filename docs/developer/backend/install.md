# Как установить Ulacos

Улакос состоит из 2 частей это API и UI. Устанавливаются они отдельно и работают как 2 разных домена (виртуальных хоста).

## Установка API (backend)

### Системные требования

Что бы выполнить условия этой инструкции, ваша среда должна соответствовать следующим параметрам

- PHP 5.6+
- Установлена MySQL 5.6+
- Установлен Node.js 4.1+
- Установлен composer

### Установка

1. Сделайте Fork на репозиторию Ulacos.

2. Колонируйте созданую ветку себе на компьютре.

3. Окройте MySql и создайте базу данных по имени `ulacos`.

4. Перейдите в командную строку, задите в корень проекта и последовательно выполните следующие команды.

		npm install 
		npm link 
		composer install 

5. Установите миграции. Миграции позволяют устанвоить текущую структуру базы данных. Для более подробной информации как работать с миграциями [читайте тут](http://docs.phinx.org/en/latest/).

    Выполните команду 
   
        php vendor/bin/phinx init
   
    У вас создастся файл `phinx.yml`. Переименуйте кго в `phinx.php` и вставте туда следующий код.
    
        <?php
        return [
            'paths'        => ['migrations' => 'migrations'],
            'environments' => [
                'default_migration_table' => 'migrations',
                'default_database'        => 'dev',
                'dev'                     => [
                    'adapter'   => 'mysql',
                    'host'      => '127.0.0.1',
                    'name'      => 'ulacos',
                    'user'      => 'root',
                    'pass'      => 'root',
                    'port'      => '3306',
                    'charset'   => 'utf8',
                    'collation' => 'utf8_general_ci',
                ]
            ]
        ];
        
    Измените данные соединения с базой. Этот же файл будет использоваться для соединения с базой самой программой.
          
        php vendor/bin/phinx migrate

6. Создайте виртуальных хост и укажте домашную директорию папку `public` в корне проекта.


Вот и все! API установлено.

## Проверка API

Есть много разных HTTP клиентов которые позволят вам отправлять запросы к серверу и просматривать ответы. Любой REST API клиент подойдет. Главное правило тут:

1. Нужно добавить заглолвок `Content-Type: application/json`
2. Слать только `POST` запросы
3. Передавать параметры в теле POST запроса как текст JSON

Но если вы пользуетесь браузером Chrome, мы рекомендуем пользоваться расширением DHC. Во превых он очень продвинут в плане возможностей, а так же позволяет загружать и выгружать точки доступа API в JSON файл.

Установите расширение Google Chrome - DHC и откройте его.

![DHC](img/dhc.png)

1. В нижнем левом углу есть кнока импорта. Нажимте ее, перейдите в корень проекта, потом `modules\App\dhc_chrome.json` и загрузите его. Группа методов API этого модуля будет загружена. Подобный файл есть в любом основном модуле Улакос. 
2. Роскройте дерево методов, и найдите App\Module Language. Кликните на нем что бы он загрузился.
3. Перед тем как нажать кнопку оправить, убедитесь что доменное имя в адресе соответствует тому что вы настроили как виртуальных хост для сервера JSON API (backend).
4. Вы должны увидеть результат выполнения метода. Если это произошло то все работает нормально. 

## Установка UI

Ну здесь нет ни чего сложного. Просто сделайте Fork на репозиторию `frontend`, сколонируйете себе на компьтер, и настройте виртуальный хостинг, в папку public.

# Обновление

Время от времени вам нужно будет обновлять ядро Ulacos. 

## Обновление API (backend)

Если вы устанеовили API через создание ветки в Git, то вам нужно сначала обновить совю ветку, которую вы отделили от основного дерева разработки.

> TODO

После это вы можете локально выполнить команду

    git pull

## Обновление UI

> TODO
