var gulp = require('gulp');
var s3 = require("gulp-s3");
var fs = require('fs');

gulp.task('sync', function() {
	var aws = JSON.parse(fs.readFileSync('./s3.json'));
	gulp.src('./site/**')
		.pipe(s3(aws));
});

gulp.task('default', ['sync']);
